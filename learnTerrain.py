#| 8 | 7 | 6
#|-----------
#| 3 | 4 | 5
#|-----------
#| 2 | 1 | 0
#V  increasing X <------
#increasing Z

import MalmoPython
import os
import sys
import time
import json
import math
import mapGenerator

sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)  # flush print output immediately

start = [0.5,4,0.5]
startYaw = 0
goal = [0,3,27]
currentYaw = startYaw
currentPos = start
turnTime = 0.5#90 degrees
walkTime = 0.233#one grid
frontGrid = 7
updateI = 10#learn by difficulty of map and distance between robot and goal
generatedTerrain = mapGenerator.generateTerrain()
generatedHill = mapGenerator.generateHill()

def Menger(xorg, yorg, zorg, size, blocktype, variant, holetype):
    #draw solid chunk
    genstring = GenCuboidWithVariant(xorg,yorg,zorg,xorg+size-1,yorg+size-1,zorg+size-1,blocktype,variant) + "\n"
    #now remove holes
    unit = size
    while (unit >= 3):
        w=unit/3
        for i in xrange(0, size, unit):
            for j in xrange(0, size, unit):
                x=xorg+i
                y=yorg+j
                genstring += GenCuboid(x+w,y+w,zorg,(x+2*w)-1,(y+2*w)-1,zorg+size-1,holetype) + "\n"
                y=yorg+i
                z=zorg+j
                genstring += GenCuboid(xorg,y+w,z+w,xorg+size-1, (y+2*w)-1,(z+2*w)-1,holetype) + "\n"
                genstring += GenCuboid(x+w,yorg,z+w,(x+2*w)-1,yorg+size-1,(z+2*w)-1,holetype) + "\n"
        unit/=3
    return genstring

def GenCuboid(x1, y1, z1, x2, y2, z2, blocktype):
    return '<DrawCuboid x1="' + str(x1) + '" y1="' + str(y1) + '" z1="' + str(z1) + '" x2="' + str(x2) + '" y2="' + str(y2) + '" z2="' + str(z2) + '" type="' + blocktype + '"/>'

def GenCuboidWithVariant(x1, y1, z1, x2, y2, z2, blocktype, variant):
    return '<DrawCuboid x1="' + str(x1) + '" y1="' + str(y1) + '" z1="' + str(z1) + '" x2="' + str(x2) + '" y2="' + str(y2) + '" z2="' + str(z2) + '" type="' + blocktype + '" variant="' + variant + '"/>'



missionXML='''<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
            <Mission xmlns="http://ProjectMalmo.microsoft.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            
              <About>
                <Summary>Hello world!</Summary>
              </About>
              
            <ServerSection>
              <ServerInitialConditions>
                <Time>
                    <StartTime>1000</StartTime>
                    <AllowPassageOfTime>false</AllowPassageOfTime>
                </Time>
                <Weather>clear</Weather>
              </ServerInitialConditions>
              <ServerHandlers>
                  <FlatWorldGenerator generatorString="3;7,2*3,2;1;village"/>
                  <DrawingDecorator>
                    '''+ generatedTerrain + generatedHill +'''
                    <DrawLine x1="-8" y1="3" z1="2" x2="-8" y2="3" z2="2" type="water"/>
                    <DrawLine x1="-7" y1="3" z1="1" x2="-7" y2="3" z2="3" type="water"/>
                    <DrawLine x1="-6" y1="3" z1="0" x2="-6" y2="3" z2="4" type="water"/>
                    <DrawLine x1="-5" y1="3" z1="-1" x2="-5" y2="3" z2="5" type="water"/>
                    <DrawLine x1="-4" y1="3" z1="0" x2="-4" y2="3" z2="4" type="water"/>
                    <DrawLine x1="-3" y1="3" z1="1" x2="-3" y2="3" z2="3" type="water"/>
                    <DrawLine x1="-2" y1="3" z1="2" x2="-2" y2="3" z2="2" type="water"/>
                    <DrawCuboid x1="-8" y1="2" z1="-1" x2="-2" y2="2" z2="5" type="water"/>
                  </DrawingDecorator>
                  <ServerQuitFromTimeUp timeLimitMs="30000"/>
                  <ServerQuitWhenAnyAgentFinishes/>
                </ServerHandlers>
              </ServerSection>
              
              <AgentSection mode="Survival">
                <Name>MalmoTutorialBot</Name>
                <AgentStart>
                    <Placement x="'''+str(start[0])+'''" y="'''+str(start[1])+'''" z="'''+str(start[2])+'''" yaw="'''+str(startYaw)+'''"/>
                    <Inventory>
                        <InventoryItem slot="8" type="diamond_pickaxe"/>
                    </Inventory>
                </AgentStart>
                <AgentHandlers>
                  <ObservationFromFullStats/>
                  <ObservationFromGrid>
                      <Grid name="floor3x3">
                        <min x="-1" y="-1" z="-1"/>
                        <max x="1" y="-1" z="1"/>
                      </Grid>
					    <Grid name="upper3x3">
                        <min x="-1" y="0" z="-1"/>
                        <max x="1" y="0" z="1"/>
                      </Grid>
                  </ObservationFromGrid>
                  <ContinuousMovementCommands turnSpeedDegs="180"/>
                  <InventoryCommands/>
                  <AgentQuitFromTouchingBlockType>
                      <Block type="diamond_block" />
                  </AgentQuitFromTouchingBlockType>
                </AgentHandlers>
              </AgentSection>
            </Mission>'''

# Create default Malmo objects:

agent_host = MalmoPython.AgentHost()
try:
    agent_host.parse( sys.argv )
except RuntimeError as e:
    print 'ERROR:',e
    print agent_host.getUsage()
    exit(1)
if agent_host.receivedArgument("help"):
    print agent_host.getUsage()
    exit(0)

my_mission = MalmoPython.MissionSpec(missionXML, True)
my_mission_record = MalmoPython.MissionRecordSpec()

# Attempt to start a mission:
max_retries = 3
for retry in range(max_retries):
    try:
        agent_host.startMission( my_mission, my_mission_record )
        break
    except RuntimeError as e:
        if retry == max_retries - 1:
            print "Error starting mission:",e
            exit(1)
        else:
            time.sleep(2)


# Loop until mission starts:
print "Waiting for the mission to start ",
world_state = agent_host.getWorldState()
while not world_state.has_mission_begun:
    sys.stdout.write(".")
    time.sleep(0.1)
    world_state = agent_host.getWorldState()
    for error in world_state.errors:
        print "Error:",error.text

print
print "Mission running ",

#find eduliance diswtance between two points
def findDistance(s,g):
    return math.sqrt((g[0]-s[0])**2 + (g[2]-s[2])**2)


#walk one grid; record current Position
def walk(terrain):
    if terrain == u'sand':
        agent_host.sendCommand("move 0.5")
        time.sleep(walkTime*2)
        agent_host.sendCommand("move 0")
    elif terrain == u'ice':
        agent_host.sendCommand("move 0.75")
        time.sleep(walkTime*1.333)
        agent_host.sendCommand("move 0")
    else:
        agent_host.sendCommand("move 1")
        time.sleep(walkTime)
        agent_host.sendCommand("move 0")

    
f = open('TerrainCost.txt','r')
costList = {}
costList = eval(f.read())
if not costList:
    costList = {u'air':(0,0)}
scalingFactor = 100 #Scale the difference
def calTerrainCost(terrain,distance,time):
    if time == -1:
        if not terrain in costList:
            print "lava"
            costList[terrain] = (999999999,-1)
    else:
        if(terrain != u'air' and distance != 0):
            cost = (time/distance)*scalingFactor
            if terrain in costList:#if terrain is exist already, modify it
                print "Modify"
                temp = costList[terrain]
                cost = ((temp[0]*temp[1])+cost)/(temp[1]+1)
                costList[terrain] = (cost,temp[1]+1)
            else:
                costList[terrain] = (cost,0)
    print costList


goalDistance = findDistance(start,goal)

agent_host.sendCommand("move 1")     #And start running...

jumping = False
terrain = "u'air'"
terrainCounter = 0
terrainTime = 0
start_time = 0



# Loop until mission ends:
while world_state.is_mission_running:
    sys.stdout.write(".")
    time.sleep(0.1)
    world_state = agent_host.getWorldState()
    for error in world_state.errors:
        print "Error:",error.text


    if world_state.number_of_observations_since_last_state > 0:
        msg = world_state.observations[-1].text
        observations = json.loads(msg)
        groundGrid = observations.get(u'floor3x3', 0)
        if groundGrid[1] == u'water':
            agent_host.sendCommand("jump 1")
            walk(terrain)
        else:
            agent_host.sendCommand("jump 0")
        if groundGrid[1] != terrain:
            terrainTime = time.time() - start_time
            print terrain
            print "Distance:"
            print terrainCounter
            print " Time:"
            print terrainTime
            print
            calTerrainCost(terrain,terrainCounter,terrainTime)
            start_time = time.time()
            terrain = groundGrid[1]
            terrainCounter = 0

        terrainCounter = terrainCounter + 1
        walk(terrain)
print groundGrid[4]
calTerrainCost(groundGrid[4],terrainCounter,-1)

f = open('TerrainCost.txt','w')
f.write(json.dumps(costList))


print
print "Mission ended"
# Mission has ended.