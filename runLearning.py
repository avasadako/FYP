import os
import mapGenerator
import time
import sys
#import learning
import pickle

sampleNum = 100#number of samples it learn from
learningTimes = 27#3*3*3#how many times it run in per sample
learningSet = [None]*sampleNum
failureCounter = 0

print "Training Start"

def generateDecision():
    result = []
    for i in range(0,3):
        for j in range(0,3):
            for k in range(0,3):
                result.append((i,j,k))
    print result
    return result

decisionList = generateDecision()
for i in range(sampleNum):
    generatedSample = mapGenerator.generateMultiSample()
    #write generated terrain sample in file, and it will read by learning.py
    sampleFile = open('sampleContainer.txt','w')
    sampleFile.write(generatedSample)
    sampleFile.close()
    fastestTime = -1#running time of compile once, save the fastest once
    fastestResult = []
    for j in range(learningTimes):
        print "Sample %d" % i
        print "Times %d" % j
        decision = decisionList[j]
        startTime = time.time()
        temp = generatedSample
        #e = os.system("learning.py")
        e = os.system("learning.py %d %d %d" % (decision[0],decision[1],decision[2]))#if e=1, error
        #read result
        f = open("decisionTemp.file","rb")
        result = pickle.load(f)
        f.close()
        endTime = time.time()
        #print result
        if result==0:#mission fail
            failureCounter = failureCounter + 1
            print "Mission Fail"
            continue
        usingTime = endTime - startTime
        if (fastestTime == -1) or (usingTime < fastestTime):
            #replace by faster one
            fastestResult = result
            fastestTime = usingTime
    learningSet[i] = fastestResult
    trainingData = open("trainingDataMulti3.file","wb")
    pickle.dump(learningSet,trainingData,pickle.HIGHEST_PROTOCOL)
    trainingData.close()

print "Failure number x out of 2700: ",failureCounter
print "Training End"