from random import randint

terrainList = {0:"water",1:"ice",2:"sand",3:"lava"}

def generateTerrain():
    result = ""
    quantity = 5#randint(5,20)
    for q in range(quantity):
        typeIndex = randint(0,3)
        type = terrainList[typeIndex]
        size = randint(4,10)
        locationX = randint(-15,15)#randint(-50,50)
        locationZ = randint(15,50)#randint(15,100)
        for int in range(size):
            x1 = locationX - int
            x2 = locationX + int
            z1 = locationZ - (size-1-int)
            z2 = locationZ + (size-1-int)
            #Format: <DrawLine x1="-8" y1="3" z1="2" x2="-8" y2="3" z2="2" type="water"/>
            line1 = "<DrawLine x1=\"" + str(x1) + "\" y1=\"3\" z1=\"" + str(z1) + "\" x2=\"" + str(x1) + "\" y2=\"3\" z2=\"" + str(z2) + "\" type=\"" + type + "\"/>"
            line2 = "<DrawLine x1=\"" + str(x2) + "\" y1=\"3\" z1=\"" + str(z1) + "\" x2=\"" + str(x2) + "\" y2=\"3\" z2=\"" + str(z2) + "\" type=\""+ type +"\"/>"
            result = result + line1 + "\n"
            result = result + line2 + "\n"
            if typeIndex == 0:
                for d in range(1,3):
                    under1 = "<DrawLine x1=\"" + str(x1) + "\" y1=\"" + str(d) + "\" z1=\"" + str(z1) + "\" x2=\"" + str(x1) + "\" y2=\"" + str(d) + "\" z2=\"" + str(z2) + "\" type=\"" + type + "\"/>"
                    under2 = "<DrawLine x1=\"" + str(x2) + "\" y1=\"" + str(d) + "\" z1=\"" + str(z1) + "\" x2=\"" + str(x2) + "\" y2=\"" + str(d) + "\" z2=\"" + str(z2) + "\" type=\""+ type +"\"/>"
                    result = result + under1 + "\n"
                    result = result + under2 + "\n"
    print result
    return result

def generateHill():
    result = ""
    quantity = randint(1,10)
    for q in range(quantity):
        size = randint(4,10)
        locationX = randint(-50,50)
        locationZ = randint(15,100)
        line = "<DrawSphere x=\"" + str(locationX) + "\" y=\"1\" z=\"" + str(locationZ) + "\" radius=\"" + str(size) + "\" type=\"grass\"/>"
        result = result + line + "\n"
    print result
    return result

def generateSample():
    #result = "<DrawCuboid x1=\"-25\" y1=\"3\" z1=\"0\" x2=\"25\" y2=\"0\" z2=\"35\" type=\"grass\"/>" + "\n"
    result = ""
    typeIndex = randint(0,3)
    type = terrainList[typeIndex]
    size = randint(4,10)
    locationX = randint(-10,10)#randint(-15,15)
    locationZ = randint(15,25)
    for int in range(size):
        x1 = locationX - int
        x2 = locationX + int
        z1 = locationZ - (size-1-int)
        z2 = locationZ + (size-1-int)
        line1 = "<DrawLine x1=\"" + str(x1) + "\" y1=\"3\" z1=\"" + str(z1) + "\" x2=\"" + str(x1) + "\" y2=\"3\" z2=\"" + str(z2) + "\" type=\"" + type + "\"/>"
        line2 = "<DrawLine x1=\"" + str(x2) + "\" y1=\"3\" z1=\"" + str(z1) + "\" x2=\"" + str(x2) + "\" y2=\"3\" z2=\"" + str(z2) + "\" type=\""+ type +"\"/>"
        result = result + line1 + "\n"
        result = result + line2 + "\n"
        if terrainList[typeIndex] == "water":
            for d in range(1,3):
                under1 = "<DrawLine x1=\"" + str(x1) + "\" y1=\"" + str(d) + "\" z1=\"" + str(z1) + "\" x2=\"" + str(x1) + "\" y2=\"" + str(d) + "\" z2=\"" + str(z2) + "\" type=\"" + type + "\"/>"
                under2 = "<DrawLine x1=\"" + str(x2) + "\" y1=\"" + str(d) + "\" z1=\"" + str(z1) + "\" x2=\"" + str(x2) + "\" y2=\"" + str(d) + "\" z2=\"" + str(z2) + "\" type=\""+ type +"\"/>"
                result = result + under1 + "\n"
                result = result + under2 + "\n"
        #under1 = "<DrawCuboid x1=\"" + str(locationX-size) + "\" y1=\"0\" z1=\"" + str(locationZ-size) + "\" x2=\"" + str(locationX+size) + "\" y2=\"2\" z2=\"" + str(locationZ+size) + "\" type=\"water\"/>"
        #under2 = "<DrawCuboid x1=\"" + str(locationX-size) + "\" y1=\"-1\" z1=\"" + str(locationZ-size) + "\" x2=\"" + str(locationX+size) + "\" y2=\"-1\" z2=\"" + str(locationZ+size) + "\" type=\"grass\"/>"
        #result = result + under1 + "\n"
        #result = result + under2 + "\n"
    return result

def generateMultiSample():
    r = "<DrawCuboid x1=\"-25\" y1=\"3\" z1=\"0\" x2=\"25\" y2=\"0\" z2=\"35\" type=\"grass\"/>" + "\n"
    r = r + generateSample()
    r = r + generateSample()
    return r

def generateGoal():
    goalX = -5#randint(-15,15)#randint(-50,50)
    goalZ = 60#randint(70,100)
    return (goalX,goalZ)
    