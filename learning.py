#| 8 | 7 | 6
#|-----------
#| 3 | 4 | 5
#|-----------
#| 2 | 1 | 0
#V  increasing X <------
#increasing Z

import MalmoPython
import os
import sys
import time
import json
import math
import convertObservation
import mapGenerator
from random import randint
import pickle

sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)  # flush print output immediately

start = [0.5,4,0.5]
startYaw = 0
goal = [0,3,37]
currentYaw = startYaw
currentPos = start
turnTime = 0.5#90 degrees
walkTime = 0.232#one grid
swimTime = 0.46#one grid
updateI = 10#learn by difficulty of map and distance between robot and goal
visionDistance = 10
decideDistance = 5#robot moves every 5 z-distance, it will make decision
iterationRate = 0.2
frontGrid =(visionDistance/2)
reward = 0
#generatedTerrain = sys.argv[1]

#Read terrain sample command from file
sampleFile = open('sampleContainer.txt','r')
generatedTerrain = sampleFile.read()
sampleFile.close()
#generatedTerrain = mapGenerator.generateSample()

def Menger(xorg, yorg, zorg, size, blocktype, variant, holetype):
    #draw solid chunk
    genstring = GenCuboidWithVariant(xorg,yorg,zorg,xorg+size-1,yorg+size-1,zorg+size-1,blocktype,variant) + "\n"
    #now remove holes
    unit = size
    while (unit >= 3):
        w=unit/3
        for i in xrange(0, size, unit):
            for j in xrange(0, size, unit):
                x=xorg+i
                y=yorg+j
                genstring += GenCuboid(x+w,y+w,zorg,(x+2*w)-1,(y+2*w)-1,zorg+size-1,holetype) + "\n"
                y=yorg+i
                z=zorg+j
                genstring += GenCuboid(xorg,y+w,z+w,xorg+size-1, (y+2*w)-1,(z+2*w)-1,holetype) + "\n"
                genstring += GenCuboid(x+w,yorg,z+w,(x+2*w)-1,yorg+size-1,(z+2*w)-1,holetype) + "\n"
        unit/=3
    return genstring

def GenCuboid(x1, y1, z1, x2, y2, z2, blocktype):
    return '<DrawCuboid x1="' + str(x1) + '" y1="' + str(y1) + '" z1="' + str(z1) + '" x2="' + str(x2) + '" y2="' + str(y2) + '" z2="' + str(z2) + '" type="' + blocktype + '"/>'

def GenCuboidWithVariant(x1, y1, z1, x2, y2, z2, blocktype, variant):
    return '<DrawCuboid x1="' + str(x1) + '" y1="' + str(y1) + '" z1="' + str(z1) + '" x2="' + str(x2) + '" y2="' + str(y2) + '" z2="' + str(z2) + '" type="' + blocktype + '" variant="' + variant + '"/>'
    
missionXML='''<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
            <Mission xmlns="http://ProjectMalmo.microsoft.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            
              <About>
                <Summary>Hello world!</Summary>
              </About>
              
            <ServerSection>
              <ServerInitialConditions>
                <Time>
                    <StartTime>1000</StartTime>
                    <AllowPassageOfTime>false</AllowPassageOfTime>
                </Time>
                <Weather>clear</Weather>
              </ServerInitialConditions>
              <ServerHandlers>
                  <FlatWorldGenerator generatorString="3;7,2*3,2;1;village"/>
                  <DrawingDecorator>
                    ''' + generatedTerrain + '''
                    <DrawBlock x="'''+str(goal[0])+'''" y="'''+str(goal[1])+'''" z="'''+str(goal[2])+'''" type="diamond_block"/>
                    <DrawCuboid x1="-2" y1="3" z1="35" x2="2" y2="3" z2="39" type="diamond_block"/>
                  </DrawingDecorator>
                  <ServerQuitFromTimeUp timeLimitMs="30000"/>
                  <ServerQuitWhenAnyAgentFinishes/>
                </ServerHandlers>
              </ServerSection>
              
              <AgentSection mode="Survival">
                <Name>MalmoTutorialBot</Name>
                <AgentStart>
                    <Placement x="'''+str(start[0])+'''" y="'''+str(start[1])+'''" z="'''+str(start[2])+'''" yaw="'''+str(startYaw)+'''"/>
                </AgentStart>
                <AgentHandlers>
                  <ObservationFromFullStats/>
                  <ObservationFromGrid>
                      <Grid name="floor11n0">
                        <min x="'''+ str(-(visionDistance/2)) +'''" y="-1" z="1"/>
                        <max x="'''+ str(visionDistance/2) +'''" y="-1" z="'''+ str(visionDistance+1) +'''"/>
                      </Grid>
                      <Grid name="floor11n90">
                        <min x="'''+ str(-(visionDistance+1)) +'''" y="-1" z="'''+ str(-(visionDistance/2)) +'''"/>
                        <max x="-1" y="-1" z="'''+ str(visionDistance/2) +'''"/>
                      </Grid>
                      <Grid name="floor11n-90">
                        <min x="1" y="-1" z="'''+ str(-(visionDistance/2)) +'''"/>
                        <max x="'''+ str(visionDistance+1) +'''" y="-1" z="'''+ str(visionDistance/2) +'''"/>
                      </Grid>
                      <Grid name="floor11n180">
                        <min x="'''+ str(-(visionDistance/2)) +'''" y="-1" z="'''+ str(-(visionDistance+1)) +'''"/>
                        <max x="'''+ str(visionDistance/2) +'''" y="-1" z="-1"/>
                      </Grid>
					  <Grid name="upper11x11">
                        <min x="'''+ str(-(visionDistance/2)) +'''" y="0" z="1"/>
                        <max x="'''+ str(visionDistance/2) +'''" y="0" z="'''+ str(visionDistance+1) +'''"/>
                      </Grid>
					  <Grid name="below1x1">
                        <min x="-1" y="-1" z="-1"/>
                        <max x="1" y="1" z="1"/>
                      </Grid>
                  </ObservationFromGrid>
                  <RewardForTouchingBlockType>
                    <Block reward="-50.0" type="lava" behaviour="onceOnly"/>
                    <Block reward="100.0" type="diamond_block" behaviour="onceOnly"/>
                  </RewardForTouchingBlockType>
                  <ContinuousMovementCommands turnSpeedDegs="180"/>
                  <InventoryCommands/>
                  <AgentQuitFromTouchingBlockType>
                      <Block type="diamond_block" />
                  </AgentQuitFromTouchingBlockType>
                </AgentHandlers>
              </AgentSection>
            </Mission>'''

# Create default Malmo objects:

agent_host = MalmoPython.AgentHost()
try:
    agent_host.parse( sys.argv )
except RuntimeError as e:
    print 'ERROR:',e
    print agent_host.getUsage()
    exit(1)
if agent_host.receivedArgument("help"):
    print agent_host.getUsage()
    exit(0)

my_mission = MalmoPython.MissionSpec(missionXML, True)
my_mission_record = MalmoPython.MissionRecordSpec()

# Attempt to start a mission:
max_retries = 3
for retry in range(max_retries):
    try:
        agent_host.startMission( my_mission, my_mission_record )
        break
    except RuntimeError as e:
        if retry == max_retries - 1:
            print "Error starting mission:",e
            exit(1)
        else:
            time.sleep(2)


# Loop until mission starts:
print "Waiting for the mission to start ",
world_state = agent_host.getWorldState()
while not world_state.has_mission_begun:
    sys.stdout.write(".")
    time.sleep(0.1)
    world_state = agent_host.getWorldState()
    for error in world_state.errors:
        print "Error:",error.text

print
print "Mission running ",

#find eduliance diswtance between two points
def findDistance(s,g):
    return math.sqrt((g[0]-s[0])**2 + (g[2]-s[2])**2)

#find goal's angle
def findGoalAngle(s,g):
    angle = math.degrees(math.atan(math.fabs(g[0]-s[0])/math.fabs(g[2]-s[2])))
    if(g[0]<0 and g[2]<0):
        angle = 180 - angle
    elif(g[0]>0 and g[2]<0):
        angle = -180 + angle
    elif(g[0]>0 and g[2]>=0):
        angle = -angle
    return angle

def findAngle(r,g):
    angle = math.degrees(math.atan(math.fabs(g[0]-r[0])/math.fabs(g[2]-r[2])))
    if(currentYaw == 90) or (currentYaw == -90):
        angle = 90 - angle
    return angle

#walk one grid; record current Position
def walk(terrain):
    if terrain == u'sand':
        #agent_host.sendCommand("jump 0")
        agent_host.sendCommand("move 0.5")
        time.sleep(walkTime*2)
        agent_host.sendCommand("move 0")
    elif terrain == u'ice':
        #agent_host.sendCommand("jump 0")
        agent_host.sendCommand("move 0.75")
        time.sleep(walkTime*1.333)
        agent_host.sendCommand("move 0")
    elif terrain == u'water':
        agent_host.sendCommand("move 1")
        agent_host.sendCommand("jump 1")
        time.sleep(swimTime)
        agent_host.sendCommand("move 0")
        #agent_host.sendCommand("jump 0")
    elif terrain == u'grass':
        #agent_host.sendCommand("jump 0")
        agent_host.sendCommand("move 1")
        time.sleep(walkTime)
        agent_host.sendCommand("move 0")
    else:
        agent_host.sendCommand("move 1")
        time.sleep(walkTime)
        agent_host.sendCommand("move 0")
    if(currentYaw == 0):
        currentPos[2] = currentPos[2]+1
    elif(currentYaw == 90):
        currentPos[0] = currentPos[0]-1
    elif(currentYaw == 180):
        currentPos[2] = currentPos[2]-1
    elif(currentYaw == -90):
        currentPos[0] = currentPos[0]+1


#Change front grid index
def changeFront():
    if currentYaw == 0:
        frontGrid = 7
    elif currentYaw == 90:
        frontGrid = 3
    elif currentYaw == 180:
        frontGrid = 1
    elif currentYaw == -90:
        frontGrid = 5

#turn 90 degrees; record current Yaw
def turn(d):
    global currentYaw
    #Left
    if d == -1:
        agent_host.sendCommand("turn -1")
        time.sleep(turnTime)
        agent_host.sendCommand("turn 0")
        currentYaw = currentYaw - 90
        if currentYaw == -180:
            currentYaw = 180
    #Right
    elif d == 1:
        agent_host.sendCommand("turn 1")
        time.sleep(turnTime)
        agent_host.sendCommand("turn 0")
        currentYaw = currentYaw + 90
        if currentYaw == 270:
            currentYaw = -90
#    changeFront()

def avoidLava(l):
    agent_host.sendCommand("move 0")
    if (groundObv[frontGrid-1]==u'lava'):
        turn(-1)
    else:
        turn(1)

#if the robot saw the goal, go directly
def sawGoal(l):
    #print "It saw the goal"
    agent_host.sendCommand("move 1")
    time.sleep(3)
    #agent_host.sendCommand("move 0")
    #turnToAngle(findAngle(currentPos,goal))
    #walk(l[frontGrid])#Should be wrong


def turnToAngle(a):
    if(currentYaw == 180):
        if (goal[2] > currentPos[2]):
            turn(1)            
    if(angleRG > 45):
        if(currentYaw == 0):
            #if(goal[2] > 0):
            if(goal[0] > currentPos[0]):#goal at left-hand side
                turn(-1)
            else:#goal at right-hand side
                turn(1)
            #if(goal[2] < 0):
            #    if(goal[0] < currentPos[0]):#goal at left-hand side
            #        turn(-1)
            #    else:#goal at right-hand side
            #        turn(1)
        elif(currentYaw == 90):
            if(goal[2] > currentPos[2]):
                turn(-1)
            else:
                turn(1)
#        elif(currentYaw == 180):
#            if(goal[2] > currentPos[2]):
#                turn(-1)
#            else:
#                turn(1)
        elif(currentYaw == -90):
            if(goal[2] > currentPos[2]):
                turn(1)
            else:
                turn(-1)
    elif(angleRG <= 45) and (currentPos[2]-goal[2])<3:#if parallel to the goal
        if(currentPos[0]>goal[0]) and currentYaw==-90:#goal is behind the robot
            turn(1)
            turn(1)
        elif(currentPos[0]<goal[0]) and currentYaw==90:
            turn(1)
            turn(1)


goalDistance = findDistance(start,goal)
goalAngle = findGoalAngle(start,goal)

#agent_host.sendCommand("move 1")     #And start running...
walk(u'grass')

jumping = False
iterationDistance = int(math.ceil(iterationRate*math.fabs(goal[2]-currentPos[2])))#int(math.ceil(iterationRate*findDistance(currentPos,goal)))
decisionCounter = 1
decisionTuple = [None]*3#([gridStatus],'decision')
# Loop until mission ends:
while world_state.is_mission_running:
    sys.stdout.write(".")
    time.sleep(0.1)
    world_state = agent_host.getWorldState()
    for error in world_state.errors:
        print "Error:",error.text


    #Do this per x loops
    if iterationDistance==0:
        angleRG = findAngle(currentPos,goal)#angle between robot current position and goal
        #print "Angle",angleRG
        turnToAngle(angleRG)
        iterationDistance = int(math.ceil(iterationRate*math.fabs(goal[2]-currentPos[2])))#int(math.ceil(iterationRate*findDistance(currentPos,goal)))
        #print "iteration:",iterationDistance
    iterationDistance = iterationDistance-1
        
    #print currentYaw

    #get observations
    if world_state.number_of_observations_since_last_state > 0:
        msg = world_state.observations[-1].text
        observations = json.loads(msg)
        if (currentYaw == 0):
            groundGrid = observations.get(u'floor11n0', 0)
        elif (currentYaw == 90):
            groundGrid = observations.get(u'floor11n90', 0)
        elif (currentYaw == -90):
            groundGrid = observations.get(u'floor11n-90', 0)
        elif (currentYaw == 180):
            groundGrid = observations.get(u'floor11n180', 0)
        belowGrid = observations.get(u'below1x1', 0)
        groundObv = convertObservation.convertObv(groundGrid,currentYaw)
        upperGrid = observations.get(u'upper11x11', 0)
        #print currentYaw
        #print groundObv	

    if u'water' in belowGrid:
        print "Inside Water"
        agent_host.sendCommand("jump 1")
    else:
        agent_host.sendCommand("jump 0")

    walk(groundObv[frontGrid])

    #Random decide(learning only)
    #decisionNum = (goal[2] - (start[2]-0.5))//10
    if (currentPos[2] > decisionCounter*10) and (goal[2]-currentPos[2]>3):	
        arg = int(sys.argv[decisionCounter])
        if arg == 0:
            choice = -1
        elif arg == 1:
            choice = 0
        elif arg == 2:
            choice = 1
        #choice = randint(-1,1)
        #if choice == -1:
        #    print "Turn left"
        #elif choice == 0:
        #    print "Go straight"
        #elif choice == 1:
        #    print "Turn right"
        decisionTuple[decisionCounter-1] = (groundObv,choice)#save training data
        turn(choice)
        #print decisionTuple[decisionCounter]
        decisionCounter = decisionCounter+1
        for x in range(3):
            walk(groundObv[visionDistance/2])

    #Avoid lava
    #if groundObv[frontGrid+(visionDistance+1)] == u'lava':
    #    avoidLava(groundObv)
            
    #Goal is near enough
    if groundObv[visionDistance/2+(visionDistance+1)*1] == u'diamond_block' or groundObv[visionDistance/2+(visionDistance+1)*2] == u'diamond_block' or groundObv[visionDistance/2+(visionDistance+1)*3] == u'diamond_block' or groundObv[visionDistance/2+(visionDistance+1)*4] == u'diamond_block' or groundObv[visionDistance/2+(visionDistance+1)*5] == u'diamond_block':
        sawGoal(groundObv)
	
    updateI = updateI-1
    #walk(groundGrid[frontGrid])

    for r in world_state.rewards:
        reward += r.getValue()

print ("reward: %d" % reward)

#Save result as file
decisionTemp = open("decisionTemp.file","wb")
if (reward > 0):#reached the goal
    pickle.dump(decisionTuple,decisionTemp,pickle.HIGHEST_PROTOCOL)
else:
    pickle.dump(0,decisionTemp,pickle.HIGHEST_PROTOCOL)
decisionTemp.close()


print
print "Mission ended"
# Mission has ended.