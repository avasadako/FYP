import pickle

trainingFile = open("trainingData4.file","rb")
trainingData = pickle.load(trainingFile)
trainingFile.close()

trainingFile = open("trainingDataMulti2.file","rb")
trainingData += pickle.load(trainingFile)
trainingFile.close()

trainingFile = open("trainingDataMulti.file","rb")
trainingData += pickle.load(trainingFile)
trainingFile.close()

trainingFile = open("trainingData3.file","rb")
trainingData += pickle.load(trainingFile)
trainingFile.close()

trainingFile = open("trainingData2.file","rb")
trainingData += pickle.load(trainingFile)
trainingFile.close()

trainingFile = open("trainingData1.file","rb")
trainingData += pickle.load(trainingFile)
trainingFile.close()

#Remove none
trainingData = [x for x in trainingData if x is not None]

print "Length", len(trainingData)

trainingDataMF = open("trainingDataMerged.file","wb")
pickle.dump(trainingData,trainingDataMF,pickle.HIGHEST_PROTOCOL)
trainingDataMF.close()
