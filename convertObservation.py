size = 11

def convertObv(ls,yaw):
    result = [None]*(size*size)
    if (yaw == 0):
        result = ls
        #print ls
        #print result
    elif (yaw == 90):
        for i in range(size,0,-1):
            for j in range(0,size,1):
                result[((size-i)*size)+j] = ls[(i-1)+(size*j)]
    elif (yaw == -90):
        for i in range(0,size,1):#0,1,2...
            for j in range(size,0,-1):#11,10,9...
                result[(i*size)+(size-j)] = ls[(size*(j-1))+i]
    elif (yaw == 180):
        for i in range(0,size*size,1):#0-120
            result[i] = ls[(size*size-1)-i]
    return result