#| 8 | 7 | 6
#|-----------
#|3 | 4 | 5
#|-----------
#| 2 | 1 | 0
#V  increasing X <------
#increasing Z

import MalmoPython
import os
import sys
import time
import json
import math

sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)  # flush print output immediately

start = [0.5,4,0.5]
startYaw = 0
goal = [0,3,27]
currentYaw = startYaw
currentPos = start
turnTime = 0.5#90 degrees
walkTime = 0.232#one grid
frontGrid = 7
updateI = 10#learn by difficulty of map and distance between robot and goal

def Menger(xorg, yorg, zorg, size, blocktype, variant, holetype):
    #draw solid chunk
    genstring = GenCuboidWithVariant(xorg,yorg,zorg,xorg+size-1,yorg+size-1,zorg+size-1,blocktype,variant) + "\n"
    #now remove holes
    unit = size
    while (unit >= 3):
        w=unit/3
        for i in xrange(0, size, unit):
            for j in xrange(0, size, unit):
                x=xorg+i
                y=yorg+j
                genstring += GenCuboid(x+w,y+w,zorg,(x+2*w)-1,(y+2*w)-1,zorg+size-1,holetype) + "\n"
                y=yorg+i
                z=zorg+j
                genstring += GenCuboid(xorg,y+w,z+w,xorg+size-1, (y+2*w)-1,(z+2*w)-1,holetype) + "\n"
                genstring += GenCuboid(x+w,yorg,z+w,(x+2*w)-1,yorg+size-1,(z+2*w)-1,holetype) + "\n"
        unit/=3
    return genstring

def GenCuboid(x1, y1, z1, x2, y2, z2, blocktype):
    return '<DrawCuboid x1="' + str(x1) + '" y1="' + str(y1) + '" z1="' + str(z1) + '" x2="' + str(x2) + '" y2="' + str(y2) + '" z2="' + str(z2) + '" type="' + blocktype + '"/>'

def GenCuboidWithVariant(x1, y1, z1, x2, y2, z2, blocktype, variant):
    return '<DrawCuboid x1="' + str(x1) + '" y1="' + str(y1) + '" z1="' + str(z1) + '" x2="' + str(x2) + '" y2="' + str(y2) + '" z2="' + str(z2) + '" type="' + blocktype + '" variant="' + variant + '"/>'
    
missionXML='''<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
            <Mission xmlns="http://ProjectMalmo.microsoft.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            
              <About>
                <Summary>Hello world!</Summary>
              </About>
              
            <ServerSection>
              <ServerInitialConditions>
                <Time>
                    <StartTime>1000</StartTime>
                    <AllowPassageOfTime>false</AllowPassageOfTime>
                </Time>
                <Weather>clear</Weather>
              </ServerInitialConditions>
              <ServerHandlers>
                  <FlatWorldGenerator generatorString="3;7,2*3,2;1;village"/>
                  <DrawingDecorator>
                    <DrawBlock x="0" y="3" z="27" type="diamond_block"/>
                    <DrawBlock x="0" y="4" z="15" type="stone"/>
                    <DrawBlock x="1" y="4" z="15" type="stone"/>
                  </DrawingDecorator>
                  <ServerQuitFromTimeUp timeLimitMs="30000"/>
                  <ServerQuitWhenAnyAgentFinishes/>
                </ServerHandlers>
              </ServerSection>
              
              <AgentSection mode="Survival">
                <Name>MalmoTutorialBot</Name>
                <AgentStart>
                    <Placement x="'''+str(start[0])+'''" y="'''+str(start[1])+'''" z="'''+str(start[2])+'''" yaw="'''+str(startYaw)+'''"/>
                    <Inventory>
                        <InventoryItem slot="8" type="diamond_pickaxe"/>
                    </Inventory>
                </AgentStart>
                <AgentHandlers>
                  <ObservationFromFullStats/>
                  <ObservationFromGrid>
                      <Grid name="floor3x3">
                        <min x="-1" y="-1" z="-1"/>
                        <max x="1" y="-1" z="1"/>
                      </Grid>
					    <Grid name="upper3x3">
                        <min x="-1" y="0" z="-1"/>
                        <max x="1" y="0" z="1"/>
                      </Grid>
                  </ObservationFromGrid>
                  <ContinuousMovementCommands turnSpeedDegs="180"/>
                  <InventoryCommands/>
                  <AgentQuitFromTouchingBlockType>
                      <Block type="diamond_block" />
                  </AgentQuitFromTouchingBlockType>
                </AgentHandlers>
              </AgentSection>
            </Mission>'''

# Create default Malmo objects:

agent_host = MalmoPython.AgentHost()
try:
    agent_host.parse( sys.argv )
except RuntimeError as e:
    print 'ERROR:',e
    print agent_host.getUsage()
    exit(1)
if agent_host.receivedArgument("help"):
    print agent_host.getUsage()
    exit(0)

my_mission = MalmoPython.MissionSpec(missionXML, True)
my_mission_record = MalmoPython.MissionRecordSpec()

# Attempt to start a mission:
max_retries = 3
for retry in range(max_retries):
    try:
        agent_host.startMission( my_mission, my_mission_record )
        break
    except RuntimeError as e:
        if retry == max_retries - 1:
            print "Error starting mission:",e
            exit(1)
        else:
            time.sleep(2)


# Loop until mission starts:
print "Waiting for the mission to start ",
world_state = agent_host.getWorldState()
while not world_state.has_mission_begun:
    sys.stdout.write(".")
    time.sleep(0.1)
    world_state = agent_host.getWorldState()
    for error in world_state.errors:
        print "Error:",error.text

print
print "Mission running ",

#find eduliance diswtance between two points
def findDistance(s,g):
    return math.sqrt((g[0]-s[0])**2 + (g[2]-s[2])**2)

#find goal's angle
def findGoalAngle(s,g):
    angle = math.degrees(math.atan(math.fabs(g[0]-s[0])/math.fabs(g[2]-s[2])))
    if(g[0]<0 and g[2]<0):
        angle = 180 - angle
    elif(g[0]>0 and g[2]<0):
        angle = -180 + angle
    elif(g[0]>0 and g[2]>=0):
        angle = -angle
    return angle

def findAngle(r,g):
    angle = math.degrees(math.atan(math.fabs(g[0]-r[0])/math.fabs(g[2]-r[2])))
    if(currentYaw == 90) or (currentYaw == -90):
        angle = 90 - angle
    return angle

#def lines_intersection(line1x1,line1z1,line1x2,line1z2,line2x1,line2z1,line2x2,line2z2):
#    xdiff = (line1x1 - line1x2, line2x1 - line2x2)
#    zdiff = (line1z1 - line1z2, line2z1 - line2z2)
#    def det(p1x,p1z,p2x,p2z):
#        return p1x*p2z - p1z*p2x
#    div = det(xdiff[0],xdiff[1],zdiff[0],zdiff[1])
#    if div == 0:
#        raise Exception('lines do not intersect')
#    d = (det(line1x1,line1z1,line1x2,line1z2),det(line2x1,line2z1,line2x2,line2z2))
#    x = det(d[0],d[1],xdiff[0],xdiff[1])/div
#    z = det(d[0],d[1],zdiff[0],zdiff[1])/div
#    return x,z

#walk one grid; record current Position
def walk(terrain):
    if terrain == u'sand':
        agent_host.sendCommand("move 0.5")
        time.sleep(walkTime*2)
        agent_host.sendCommand("move 0")
    elif terrain == u'ice':
        agent_host.sendCommand("move 0.75")
        time.sleep(walkTime*1.333)
        agent_host.sendCommand("move 0")
    else:
        agent_host.sendCommand("move 1")
        time.sleep(walkTime)
        agent_host.sendCommand("move 0")
    if(currentYaw == 0):
        currentPos[2] = currentPos[2]+1
    elif(currentYaw == 90):
        currentPos[0] = currentPos[0]-1
    elif(currentYaw == 180):
        currentPos[2] = currentPos[2]-1
    elif(currentYaw == -90):
        currentPos[0] = currentPos[0]+1

#Change front grid index
def changeFront():
    if currentYaw == 0:
        frontGrid = 7
    elif currentYaw == 90:
        frontGrid = 3
    elif currentYaw == 180:
        frontGrid = 1
    elif currentYaw == -90:
        frontGrid = 5

#turn 90 degrees; record current Yaw
def turn(d):
    global currentYaw
    #Left
    if d == -1:
        agent_host.sendCommand("turn -1")
        time.sleep(turnTime)
        agent_host.sendCommand("turn 0")
        currentYaw = currentYaw - 90
        if currentYaw == -180:
            currentYaw = 180
    #Right
    elif d == 1:
        agent_host.sendCommand("turn 1")
        time.sleep(turnTime)
        agent_host.sendCommand("turn 0")
        currentYaw = currentYaw + 90
        if currentYaw == 270:
            currentYaw = -90
    changeFront()


goalDistance = findDistance(start,goal)
goalAngle = findGoalAngle(start,goal)

agent_host.sendCommand("move 1")     #And start running...

jumping = False
# Loop until mission ends:
while world_state.is_mission_running:
    sys.stdout.write(".")
    time.sleep(0.1)
    world_state = agent_host.getWorldState()
    for error in world_state.errors:
        print "Error:",error.text

    #Tune
#    if(updateI == 0):
#        agentToGoal = findDistance(goal,currentPos)
#        agentToStart = findDistance(start,currentPos)
#        if(agentToGoal > goalDistance):#Robot behind start
#        elif(agentToStart > goalDistance):#Robot over goal
#        updateI = 10
    angleRG = findAngle(currentPos,goal)
    print angleRG
    if(angleRG > 45):
        print "Enter"
        if(currentYaw == 0):
            #if(goal[2] > 0):
            if(goal[0] > currentPos[0]):#goal at left-hand side
                turn(-1)
            else:#goal at right-hand side
                turn(1)
            #if(goal[2] < 0):
            #    if(goal[0] < currentPos[0]):#goal at left-hand side
            #        turn(-1)
            #    else:#goal at right-hand side
            #        turn(1)
        elif(currentYaw == 90):
            if(goal[0] > currentPos[0]):
                turn(1)
            else:
                turn(-1)
        elif(currentYaw == 90):
            if(goal[2] > currentPos[2]):
                turn(-1)
            else:
                turn(1)
        elif(currentYaw == -90):
            if(goal[2] > currentPos[2]):
                turn(1)
            else:
                turn(-1)

    if world_state.number_of_observations_since_last_state > 0:
        msg = world_state.observations[-1].text
        observations = json.loads(msg)
        groundGrid = observations.get(u'floor3x3', 0)
        upperGrid = observations.get(u'upper3x3', 0)
		
        #Avoid Obstacle
        if upperGrid[frontGrid]!=u'air':
            if upperGrid[8]==u'air':
                turn(-1)
                walk(groundGrid[frontGrid])
                turn(1)
                #agent_host.sendCommand("strafe -1")
                #time.sleep(0.3)
                #agent_host.sendCommand("strafe 0")
            else:
                turn(1)
                walk(groundGrid[frontGrid])
                turn(-1)
                #agent_host.sendCommand("strafe 1")
                #time.sleep(0.3)
                #agent_host.sendCommand("strafe 0")
	
    updateI = updateI-1
    walk(groundGrid[frontGrid])

print
print "Mission ended"
# Mission has ended.